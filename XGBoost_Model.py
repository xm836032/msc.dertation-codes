import xgboost as xgb

param = {
    'max_depth': 6,
    'eta': 0.3,
    'objective': 'binary:logistic'
}
num_round = 20

def train_predict_for_label(X_train, y_train_label, X_test):
    """
    Train an XGBoost classifier for a single label and predict on the test data.
    
    Parameters:
    - X_train: Training data features
    - y_train_label: Training labels for a specific class (binary)
    - X_test: Test data features
    
    Returns:
    - y_pred: Binary predictions for the given label on test data
    """
    dtrain = xgb.DMatrix(X_train, label=y_train_label)
    dtest = xgb.DMatrix(X_test)
    bst = xgb.train(param, dtrain, num_round)
    y_pred_prob = bst.predict(dtest)
    
    return y_pred_prob > 0.5  # Convert probabilities to binary outcomes
