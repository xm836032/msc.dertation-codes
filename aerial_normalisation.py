

import os
import numpy as np
import cv2

# Dictionary containing global band means and stds
global_norm_params = {
    "band_means": np.array([151.26809261440323, 93.1159469148246, 85.05016794624635, 81.0471576353153]),
    "band_stds": np.array([48.70879149145466, 33.59622314610158, 28.000497087051126, 33.683983599997724]),
}

# Set containing the names of valid folders
aerial_folders_name = {
        'aerial_60m_cleared',
        'aerial_60m_abies_alba',
        'aerial_60m_acer_pseudoplatanus',
        'aerial_60m_alnus_spec',
        'aerial_60m_betula_spec',
        'aerial_60m_fagus_sylvatica',
        'aerial_60m_fraxinus_excelsior',
        'aerial_60m_larix_decidua',
        'aerial_60m_larix_kaempferi',
        'aerial_60m_picea_abies',
        'aerial_60m_pinus_nigra',
        'aerial_60m_pinus_strobus',
        'aerial_60m_pinus_sylvestris',
        'aerial_60m_populus_spec',
        'aerial_60m_prunus_spec',
        'aerial_60m_pseudotsuga_menziesii',
        'aerial_60m_quercus_petraea',
        'aerial_60m_quercus_robur',
        'aerial_60m_quercus_rubra',
        'aerial_60m_tilia_spec'}


def normalize_images(input_directory, output_directory):
    # Create or validate the single output directory
    aerial_output_dir = os.path.join(output_directory, "aerial_new")
    os.makedirs(aerial_output_dir, exist_ok=True)

    for subdir, _, files in os.walk(input_directory):
        class_name = os.path.basename(subdir)

        # Skip the root directory
        if class_name == os.path.basename(input_directory):
            continue

        # If the folder name is not in our list of valid names, skip it
        if class_name not in aerial_folders_name:
            print(f"Skipping class {class_name} as it's not in the aerial folders name set")
            continue
        
        # Fetch the global mean and std for normalization
        mean, std = global_norm_params["band_means"], global_norm_params["band_stds"]

        for file in files:
            if file.lower().endswith(".tif"):
                image_path = os.path.join(subdir, file)
                image_data = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)

                # Ensure the image data is in (h, w, c) format
                if image_data.shape[2] != len(mean):
                    print(f"Image {file} does not have the expected number of channels and will be skipped.")
                    continue

                # Normalize the image data with the fetched mean and std
                normalized_image_data = (image_data - mean) / std

                # Save the normalized image to the single output directory
                output_path = os.path.join(aerial_output_dir, file)  # Keep the filename unchanged
                cv2.imwrite(output_path, normalized_image_data)

    # Once all images are saved, create a zip archive
    with zipfile.ZipFile(os.path.join(output_directory, 'aerial.zip'), 'w') as zipf:
        for root, _, files in os.walk(aerial_output_dir):
            for file in files:
                file_path = os.path.join(root, file)
                zipf.write(file_path, os.path.basename(file_path))  # Use the basename to avoid directory structure in the zip

input_directory = "C:/Users/Dell XPS/OneDrive/Documents/Data"
output_directory = "C:/Users/Dell XPS/OneDrive/Documents/Data/Normalized"
normalize_images(input_directory, output_directory)
