import torch.nn as nn
import torchvision.models as torchvision_models
import torch.nn.functional as F

class CustomResnet(nn.Module):
    def __init__(self, n_bands, num_classes=15):  # Add num_classes parameter
        super(CustomResnet, self).__init__()
        self.resnet = torchvision_models.resnet18(pretrained=False)

        # Replace the first convolution layer to handle custom number of bands
        self.resnet.conv1 = nn.Conv2d(n_bands, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)

        # Add a final linear layer to output class logits
        self.fc = nn.Linear(512, num_classes)
        self.dropout = nn.Dropout(0.5)

    def forward(self, x):
        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        x = self.resnet.maxpool(x)

        x = self.resnet.layer1(x)
        x = self.resnet.layer2(x)
        x = self.resnet.layer3(x)
        x = self.resnet.layer4(x)

        x = self.resnet.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.dropout(x)
        x = self.fc(x)  # Pass through the final linear layer
        return x
