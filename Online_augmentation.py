
import torch
from torchvision import transforms
import random

class CustomTransformTensors:
    def __init__(self, transform_dict):
        self.horizontal_flip_prob = transform_dict.get('horizontal_flip', {}).get('prob', 0)
        self.vertical_flip_prob = transform_dict.get('vertical_flip', {}).get('prob', 0)
        self.rotate_image_prob = transform_dict.get('rotate_image', {}).get('prob', 0)
        self.rotate_degrees = transform_dict.get('rotate_image', {}).get('degrees', [0, 0])

    def horizontal_flip(self, img):
        return torch.flip(img, dims=[2])

    def vertical_flip(self, img):
        return torch.flip(img, dims=[1])

    def rotate_image(self, img, angle):
        return transforms.functional.rotate(img, angle)

    def __call__(self, img):
        if random.random() < self.horizontal_flip_prob:
            img = self.horizontal_flip(img)
        if random.random() < self.vertical_flip_prob:
            img = self.vertical_flip(img)
        if random.random() < self.rotate_image_prob:
            img = self.rotate_image(img, random.choice(self.rotate_degrees))
        return img

if __name__ == "__main__":
    # Test the custom transformation on a sample tensor
    transform_dict = {
        'horizontal_flip': {'prob': 0.3},
        'vertical_flip': {'prob': 0.3},
        'rotate_image': {'degrees': [30, 30], 'prob': 0.3}
    }
    transformer = CustomTransformTensors(transform_dict)
    sample_tensor = torch.rand((3, 32, 32)) #modify according to datasets
    transformed_tensor = transformer(sample_tensor)
    print(transformed_tensor.shape)
