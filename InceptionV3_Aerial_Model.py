from keras.applications.inception_v3 import InceptionV3
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D, Dropout, BatchNormalization
from keras.callbacks import LearningRateScheduler
import tensorflow as t

def build_inception_model(input_shape, num_classes=15):
    # Load the InceptionV3 model with ImageNet weights
    base_model = InceptionV3(weights='imagenet', include_top=False, input_shape=input_shape)

    # Add custom layers
    x = base_model.output
    x = BatchNormalization()(x)
    x = GlobalAveragePooling2D()(x)
    x = Dropout(0.5)(x)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.4)(x)
    predictions = Dense(units=num_classes, activation='sigmoid')(x)

    # Construct the full model
    model = Model(inputs=base_model.input, outputs=predictions)
    
    return model

class F1Score(t.keras.metrics.Metric):
    def __init__(self, name='f1_score', **kwargs):
        super(F1Score, self).__init__(name=name, **kwargs)
        self.precision = t.keras.metrics.Precision()
        self.recall = t.keras.metrics.Recall()

    def update_state(self, y_true, y_pred, sample_weight=None):
        self.precision.update_state(y_true, y_pred, sample_weight)
        self.recall.update_state(y_true, y_pred, sample_weight)

    def result(self):
        precision = self.precision.result()
        recall = self.recall.result()
        return 2 * ((precision * recall) / (precision + recall + t.keras.backend.epsilon()))

    def reset_states(self):
        self.precision.reset_states()
        self.recall.reset_states()
