import torch.nn as nn
import torchvision.models as torchvision_models

class CustomResnet1D(nn.Module):
    def __init__(self, input_channels, num_classes=15, p_drop=0.3):
        super(CustomResnet1D, self).__init__()

        # Obtain a basic 2D ResNet-18 without pre-trained weights
        resnet2d = torchvision_models.resnet18(pretrained=False)

        # Convert the 2D ResNet to 1D and set the number of input channels
        self.resnet = self.to_resnet1d(resnet2d, input_channels)

        self.resnet.fc = nn.Sequential(
            nn.Linear(512, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(512, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Dropout(p_drop),
            nn.Linear(512, num_classes)
        )

    def to_resnet1d(self, model, input_channels):

        model.conv1 = nn.Conv1d(input_channels, 64, kernel_size=7, stride=2, padding=3, bias=False)

        for name, child in model.named_children():
            if name == 'conv1':
                continue

            if isinstance(child, nn.Conv2d):
                out_channels = child.out_channels
                kernel_size = child.kernel_size[0]
                setattr(model, name, nn.Conv1d(child.in_channels, out_channels, kernel_size=kernel_size, stride=child.stride[0], padding=child.padding[0]))

            elif isinstance(child, nn.BatchNorm2d):
                setattr(model, name, nn.BatchNorm1d(child.num_features))
            elif isinstance(child, nn.AdaptiveAvgPool2d):
                setattr(model, name, nn.AdaptiveAvgPool1d(1))
            else:
                self.to_resnet1d(child, input_channels)
        return model

    def forward(self, x):

        x = x.permute(0, 3, 1, 2).contiguous()  # Transpose to [batch_size, input_channels, 6, 6]
        x = x.view(x.size(0), x.size(1), -1)  # Reshape to [batch_size, input_channels, 36]

        return self.resnet(x)
