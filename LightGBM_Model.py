import lightgbm as ltb
from sklearn.multioutput import MultiOutputClassifier

def create_lgbm_classifier():
    clf = ltb.LGBMClassifier(
        boosting_type='gbdt',
        num_leaves=31,
        max_depth=-1,
        learning_rate=0.1,
        n_estimators=100,
        n_jobs=-1  # Use all available CPU cores
    )

    return MultiOutputClassifier(clf)
