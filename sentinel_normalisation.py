import os
import numpy as np
import rasterio

data_info = {
    "s1/60m": {
        "means": [-6.933713050794077, -12.628564056094067, 0.47448312147709354],
        "stds": [87.8762246957811, 47.03070478433704, 1.297291303623673]
    }
    "s2/60m": {
        "mean": [231.43385024546893, 376.94788434611434, 241.03688288984037, 2809.8421354087955, 616.5578221193639, 2104.3826773960823, 2695.083864757169, 2969.868417923599, 1306.0814241837832, 587.0608264363341, 249.1888624097736, 2950.2294375352285],
        "std": [123.16515044781909, 139.78991338362886, 140.6154081184225, 786.4508872594147, 202.51268536579394, 530.7255451201194, 710.2650071967689, 777.4421400779165, 424.30312334282684, 247.21468849049668, 122.80062680549261, 702.7404237034002]
    }
}


base_path = r"C:\Users\Dell XPS\OneDrive\Documents\Data"


def normalize_images(directory, mean, std, normalized_directory):
    if not os.path.exists(normalized_directory):
        os.makedirs(normalized_directory)
    for filename in os.listdir(directory):
        if filename.endswith('.tif'):
            with rasterio.open(os.path.join(directory, filename)) as src:
                img = src.read()

                for band in range(img.shape[0]):
                    img[band, :, :] = (img[band, :, :] - mean[band]) / std[band]

                if "s1" in directory.lower() and img.shape[0] == 3:
                    img = np.transpose(img, (1, 2, 0))

                profile = src.profile.copy()
                with rasterio.open(os.path.join(normalized_directory, filename), 'w', **profile) as dst:
                    if "s1" in directory.lower() and img.shape[-1] == 3:
                        for band in range(img.shape[-1]):
                            dst.write(img[:, :, band], indexes=band+1)
                    else:
                        dst.write(img)
                        

    for filename in os.listdir(directory):
        if filename.endswith('.tif'):
            with rasterio.open(os.path.join(directory, filename)) as src:
                img = src.read()

                for band in range(img.shape[0]):
                    img[band, :, :] = (img[band, :, :] - mean[band]) / std[band]

                if "s2" in directory.lower() and img.shape[0] == 12:
                    img = np.transpose(img, (1, 2, 0))

                profile = src.profile.copy()
                with rasterio.open(os.path.join(normalized_directory, filename), 'w', **profile) as dst:
                    if "s2" in directory.lower() and img.shape[-1] == 12:
                        for band in range(img.shape[-1]):
                            dst.write(img[:, :, band], indexes=band+1)
                    else:
                        dst.write(img)

base_normalized_directory = r"C:\Users\Dell XPS\OneDrive\Documents\Data\Normalized"

for data_name, data in data_info.items():
    directory = os.path.join(base_path, data_name)
    mean = np.array(data["mean"])
    std = np.array(data["std"])

    normalized_directory = os.path.join(base_normalized_directory, data_name)
    normalize_images(directory, mean, std, normalized_directory).astype(np.float32)
